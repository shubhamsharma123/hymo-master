from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
from django.db import transaction
from .roleModel import *
from simple_history.models import HistoricalRecords
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin, BaseUserManager
)

from django.conf import settings


# from django.contrib.auth.models import User as UserModel


class UserManager(BaseUserManager):

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email,and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        try:
            with transaction.atomic():
                user = self.model(email=email, **extra_fields)
                user.set_password(password)
                user.save(using=self._db)
                return user
        except:
            raise

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        return self._create_user(email, password=password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.
 
    """
    # id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=25, blank=True)
    middle_name = models.CharField(max_length=25, blank=True)
    last_name = models.CharField(max_length=25, blank=True)
    phone_no = models.CharField(max_length=17, help_text='Contact phone number')
    email = models.EmailField(max_length=254, blank=True, null=True, unique=True)
    password = models.CharField(max_length=254, blank=True, null=True)
    gender = models.CharField(max_length=6, blank=True, null=True, help_text='Male, Female, Other')
    dob = models.DateField(blank=True, null=True)
    role = models.ForeignKey(Role, on_delete=models.CASCADE, help_text='1. Admin, 2. Doctor, 3. Lab Attendent, 4. Patient')
    # phone_no = models.CharField(validators=[phone_regex], max_length=17, blank=True, null=True, help_text='Contact phone number')
    address = models.TextField(max_length=255, blank=True, null=True)
    longitude = models.TextField(max_length=80, blank=True, null=True)
    latitude = models.TextField(max_length=80, blank=True, null=True)
    visit_start_time = models.TimeField(blank=True, null=True)
    visit_end_time = models.TimeField(blank=True, null=True)
    image = models.ImageField(upload_to='user/', blank=True, null=True)
    image_name = models.CharField(max_length=100, blank=True, null=True)
    about_us = models.TextField(blank=True, null=True, help_text='user_description and about_us')
    patient_check_time = models.TimeField(blank=True, null=True)
    # user_type = models.IntegerField(blank=True, null=True, help_text='1 Means Regular, 2 Means Prime User')
    # status = models.IntegerField(blank=True, null=True, help_text='1. Request Sent,2. Request Accepted, 3. Request Rejected, 4. Invitation Sent, 5. Customer, 6. Employee')
    
    is_active = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)
    otp_varification = models.BooleanField(default=False)

    otp = models.CharField(max_length=6, blank=True, null=True)
    otp_send_time = models.DateTimeField(blank=True, null=True)
    
    # is_staff = models.BooleanField(default=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    history = HistoricalRecords(table_name='user_history')

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)

    class Meta:
        db_table = 'auth_user'
        indexes = [
            models.Index(fields=['id', 'first_name', 'last_name', 'email', 'is_active'])
        ]
