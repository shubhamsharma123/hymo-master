from rest_framework import serializers
from api.models import (User)

from django.core.exceptions import ValidationError
from docapp.settings import TIME12HRSFORMAT, DATEFORMAT


class UserLoginDetailSerializer(serializers.ModelSerializer):
    """
    Return the details of Login User.
    """
    # dob = serializers.DateField(format=DATEFORMAT, input_formats=[DATEFORMAT])

    class Meta(object):
        model = User
        fields = (
        'id', 'email', 'first_name', 'last_name', 'phone_no', 'is_active', 'is_deleted',)

class UserCreateUpdateSerializer(serializers.ModelSerializer):
    """
    create/update user .
    """
    image = serializers.ImageField(required = False, allow_null=True)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'middle_name', 'last_name', 'phone_no', 'email', 'password', 'otp', 'role', 'gender', 'dob', 'address', 'longitude', 'latitude', 'visit_start_time', 'visit_end_time', 'image', 'image_name', 'about_us', 'patient_check_time', 'address')
        extra_kwargs = {
            'password': {'write_only': True},
            'otp': {'write_only': True}
        }        

    def create(self, validated_data):
        user = User()
        user.first_name = validated_data['first_name']
        user.middle_name = validated_data['middle_name']
        user.last_name = validated_data['last_name']
        user.phone_no = validated_data['phone_no']
        user.email = validated_data['email']
        user.set_password(validated_data['password'])
        user.gender = validated_data['gender']
        user.role = validated_data['role']
        user.dob = validated_data['dob']
        user.address = validated_data['address']
        if 'image' in validated_data:
            user.image = validated_data['image']

        if 'longitude' in validated_data:
            user.longitude = validated_data['longitude']
        if 'latitude' in validated_data:
            user.latitude = validated_data['latitude']
        if 'visit_start_time' in validated_data:            
            user.visit_start_time = validated_data['visit_start_time']
        if 'visit_end_time' in validated_data:    
            user.visit_end_time = validated_data['visit_end_time']
        user.about_us = validated_data['about_us']
        if 'patient_check_time' in validated_data:
            user.patient_check_time = validated_data['patient_check_time']
        user.is_active = True
        user.save()
        if 'image' in validated_data:
            self.update_image_name (user)

        return user

    def update_image_name(self, instance):
        # update uploaded image name
        image_url = str (instance.image)
        image_name = image_url.replace ('customer/', '')
        instance.image_name = image_name
        instance.save ()
