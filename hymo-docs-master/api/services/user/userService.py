from rest_framework import status
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from django.template.loader import render_to_string
from twilio.rest import Client
import json
import base64
import random
from django.core.mail import send_mail
from django.core.exceptions import ValidationError
from django.contrib.auth import authenticate, login

import pytz
from datetime import datetime, timedelta
import jwt
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

from django.core.files.base import ContentFile

from .userBaseService import UserBaseService
from api.utils.messages.userMessages import *
from api.models import User, UserSession
from api.serializers.user import (UserLoginDetailSerializer,
                                  UserCreateUpdateSerializer)

from docapp import settings

class UserService(UserBaseService):
    """
    Allow any user (authenticated or not) to access this url 
    """

    def __init__(self):
        pass

    def login(self, request, format=None):

        validated_data = self.validate_auth_data(request)

        username = request.data['email']
        username = username.lower()
        password = request.data['password']

    
        user = self.user_authenticate(username, password)
        
        if user is not None:
            if user.otp_varification is True:

                login(request, user)

                serializer = UserLoginDetailSerializer(user)

                payload = jwt_payload_handler(user)
                token = jwt.encode(payload, settings.SECRET_KEY)

                user_details = serializer.data
                user_details['token'] = token
                # User.objects.filter(pk=user.pk).update(auth_token=token)

                user_session = self.create_update_user_session(user, token, request)

                return ({"data": user_details,"code": status.HTTP_200_OK,"message": "LOGIN_SUCCESSFULLY"})
            return ({"data":None, "code":status.HTTP_400_BAD_REQUEST, "message":"your account is not varified. please firstly verify to your account please press below button send otp to verify your account."})
        return ({"data": None,"code": status.HTTP_400_BAD_REQUEST, "message": "INVALID_CREDENTIALS"})

    def user_authenticate(self, user_name, password):
        try:
            user = User.objects.get(email=user_name)
            if user.check_password(password):
                return user # return user on valid credentials
        except User.DoesNotExist:
            try:
                user = User.objects.get(phone_no=user_name)
                if user.check_password(password):
                    return user # return user on valid credentials
            except User.DoesNotExist:
                return None

    def validate_auth_data(self, request):
        error = {}
        if not request.data.get('email'):
            error.update({'email' : "FIELD_REQUIRED" })

        if not request.data.get('password'):
            error.update({'password' : "FIELD_REQUIRED" })

        if request.headers.get('device-type')=='android'or request.headers.get('device-type')=='ios':
            if not request.data.get('device_id'):
                error.update({'device_id': "FIELD_REQUIRED"})

        if error:
            raise ValidationError(error)
    
    def create_update_user_session(self, user, token, request):
        """
        Create User Session
        """
        print(request.headers.get('device-type'))
        print(request.data.get('device_id'))

        user_session = self.get_user_session_object(user.pk, request.headers.get('device-type'), request.data.get('device_id'))

        if user_session is None:
            UserSession.objects.create(
                user = user,
                token = token,
                device_id = request.data.get('device_id'),
                device_type = request.headers.get('device-type'),
                app_version = request.headers.get('app-version')
            )

        else:
            user_session.token = token
            user_session.app_version = request.headers.get('app-version')
            user_session.save()

        return user_session

    
    def get_user_session_object(self, user_id, device_type, device_id=None):
        try:
            if device_id:
                try:
                    return UserSession.objects.get(user=user_id, device_type=device_type, device_id=device_id)
                except UserSession.DoesNotExist:
                    return None

            return UserSession.objects.get(user=user_id, device_type=device_type, device_id=device_id)

        except UserSession.DoesNotExist:
            return None


    def sign_up(self, request, format=None):
        self.validate_signup_data(request.data)
        serializer = UserCreateUpdateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            user = User.objects.get (id=serializer.data.get ('id'))
            self.send_otp(user)
            return ({"data":serializer.data, "code":status.HTTP_201_CREATED, "message":"User Created Successfully"})
        #if not valid
        return ({"data":serializer.errors, "code":status.HTTP_400_BAD_REQUEST, "message":"Oops! Something went wrong."})
    
    def validate_signup_data(self, data):
        try:
            user = User.objects.get(phone_no=data.get('phone_no'))
            raise ValidationError({"phone_no": "you are already registered with this phone no. if you not remeber your password then click on Forgot Passowrd!!"})
        except User.DoesNotExist:
            return None

    def send_otp(self, user):
        try:
            tz = pytz.timezone ('Asia/Kolkata')
            current_time = datetime.now (tz)

            # user = self.get_object_by_email (email)
            otp = random.randint (100000, 999999)
            body_msg = 'Your OTP is {} . OTP is valid for 1 hour or 1 successfull attempt.'.format (
                otp)
            account_sid = "AC3c90cc753940be88466d44fa3066cf23"
            auth_token = "fdc70d87eac59a4074bc9cf54d0cc336"
            client = Client(account_sid, auth_token)
            message = client.messages.create(
                    to="+91{}".format("8146664616"), 
                    from_="+18152013322",
                    body=body_msg)

            user.otp = otp
            user.otp_send_time = current_time
            user.save ()

            
        except Exception as e:
            raise ValidationError(e)
    
    def send_otp_for_old_user(self, request, format=None):
        try:
            tz = pytz.timezone ('Asia/Kolkata')
            current_time = datetime.now (tz)
            try:
                user = User.objects.get(phone_no=request.data.get('phone_no'))
            except User.DoesNotExist:
                raise ValidationError({"error":"Please Enter valid phone_no"})

            otp = random.randint (100000, 999999)
            body_msg = 'Your OTP is {} . OTP is valid for 1 hour or 1 successfull attempt.'.format (
                otp)
            account_sid = "AC3c90cc753940be88466d44fa3066cf23"
            auth_token = "fdc70d87eac59a4074bc9cf54d0cc336"
            client = Client(account_sid, auth_token)
            message = client.messages.create(
                    to="+91{}".format("8146664616"), 
                    from_="+18152013322",
                    body=body_msg)

            user.otp = otp
            user.otp_send_time = current_time
            user.save ()

            
        except Exception as e:
            raise ValidationError(e)

        return ({"data":None, "code":status.HTTP_200_OK, "message":"OTP Sent Successfully"})

    def verify_otp(self, request, format=None):
        # self.validate_otp_data (request.data)
        tz = pytz.timezone ('Asia/Kolkata')
        current_time = datetime.now (tz)
        now_date = current_time.strftime ('%m/%d/%y')
        now_time = current_time.strftime ('%H:%M')

        id = request.data['id']
        otp = request.data['otp']

        try:
            user = User.objects.get(id=id)
        except User.DoesNotExist:
            user = None

        if user:
            if user.otp_varification is False:
                otp_send_time = user.otp_send_time
                otp_send_time = otp_send_time.astimezone (tz) + timedelta (hours=1)

                otp_date = datetime.strftime (otp_send_time, '%m/%d/%y')
                otp_time = datetime.strftime (otp_send_time, '%H:%M')

                if now_date == otp_date and now_time <= otp_time:
                    user.otp_varification = True
                    user.save()
                    return {"data": None, "code": status.HTTP_200_OK, "message": OTP_VERIFID}
                else:
                    return {"data": None, "code": status.HTTP_400_BAD_REQUEST, "message": OTP_EXPIRED}
            else:
                return ({"data":None, "code":status.HTTP_400_BAD_REQUEST, "message":"This number is already verified."})        
        else:
            return {"data": None, "code": status.HTTP_400_BAD_REQUEST, "message": DETAILS_INCORRECT}

    def logout(self, request, format=None):

        validated_data = self.validate_logout_data(request)
        try:
            jwt_token_str = request.META['HTTP_AUTHORIZATION']
            jwt_token = jwt_token_str.replace('Bearer', '')
            user_detail = jwt.decode(jwt_token, None, None)
            user = User.objects.get(pk=user_detail['user_id'])

            user_session_instance = self.get_user_session_object(user.pk, request.headers.get('device-type'), request.data.get('device_id'))

            if user_session_instance:
                user_session = self.create_update_user_session(user, None, request)
                return ({"data": None, "code": status.HTTP_200_OK, "message": "LOGOUT_SUCCESSFULLY"})
            else:
                return ({"data":None, "code":status.HTTP_400_BAD_REQUEST, "message":"RECORD_NOT_FOUND"})

        except User.DoesNotExist:
            return ({"data": None, "code": status.HTTP_400_BAD_REQUEST, "message": "RECORD_NOT_FOUND"})