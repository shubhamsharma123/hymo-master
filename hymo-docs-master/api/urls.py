from django.urls import path
from .views import *

app_name = 'api'

urlpatterns = [
    path('auth/login', LoginView.as_view(), name='login'),
    path('auth/logout/', LogoutView.as_view(), name='logout'),
    path('auth/sign-up/', SignupView.as_view(), name='auth-sign-up'),
    
    #OTP
    path('user/verify-otp/', VarifyOtpView.as_view(), name="verify-otp"),
    path('user/send-otp-to-old-user/', SendOtpForOldUser.as_view(), name="send-otp-to-old-user"),
    
    #role
    path('role/get-all-roles/', RoleListView.as_view(), name="get-all-role"),
    
]
