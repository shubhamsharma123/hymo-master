from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.schemas import AutoSchema
from rest_framework.compat import coreapi, coreschema, uritemplate

from api.services.user import UserService

userService = UserService()

class SignupView(APIView):
    permission_classes = (AllowAny,)
    schema = AutoSchema(manual_fields=[
        coreapi.Field(
            "email",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "phone_no",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "password",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "first_name",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "last_name",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "middle_name",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "gender",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "dob",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "role",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "longitude",
            required=False,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "latitude",
            required=False,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "visit_start_time",
            required=False,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "visit_end_time",
            required=False,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "about_us",
            required=False,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "patient_check_time",
            required=False,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "address",
            required=True,
            location="form",
            schema=coreschema.String()
        )
    ])
    def post(self, request, format=None):
        """
        Create User/ Signup User
        """
        result = userService.sign_up(request, format=None)
        return Response(result, status=status.HTTP_200_OK)

class VarifyOtpView(APIView):
    permission_classes = (AllowAny,)
    schema = AutoSchema(manual_fields=[
        coreapi.Field(
            "id",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "otp",
            required=True,
            location="form",
            schema=coreschema.String()
        )
    ])
    def post(self, request, format=None):
        """
        verify otp
        """
        result = userService.verify_otp(request, format=None)
        return Response(result, status=status.HTTP_200_OK)

class SendOtpForOldUser(APIView):
    permission_classes = (AllowAny,)
    schema = AutoSchema(manual_fields=[
        coreapi.Field(
            "phone_no",
            required=True,
            location="form",
            schema=coreschema.String()
        )
    ])
    def post(self, request, format=None):
        """
        Send OTP
        """
        result = userService.send_otp_for_old_user(request, format=None)
        return Response(result, status=status.HTTP_200_OK)

class LoginView(APIView):
    permission_classes = (AllowAny,)
    schema = AutoSchema(manual_fields=[
        coreapi.Field(
            "email",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "password",
            required=True,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "device_id",
            required=False,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "device-type",
            required=False,
            location="header",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "app-version",
            required=False,
            location="header",
            schema=coreschema.String()
        )
    ])

    def post(self, request, format=None):
        """
        Login
        """
        result = userService.login(request, format=None)
        return Response(result, status=status.HTTP_200_OK)


class LogoutView(APIView):
    """
    Logout
    """
    schema = AutoSchema(manual_fields=[

        coreapi.Field(
            "device-type",
            required=False,
            location="header",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "device_id",
            required=False,
            location="form",
            schema=coreschema.String()
        ),
        coreapi.Field(
            "app-version",
            required=False,
            location="header",
            schema=coreschema.String()
        )

    ])

    def post(self, request, format=None):
        # simply delete the token to force a login
        result = userService.logout(request, format=None)
        return Response(result, status=status.HTTP_200_OK)
